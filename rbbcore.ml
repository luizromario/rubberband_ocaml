open Array
open List

type rbbExpr =
    (* Literals *)
    | RbbEmptyExpr
    | RbbNumberExpr of int
    | RbbSymbolExpr of string
    | RbbArrayExpr of rbbExpr list
    | RbbBlockExpr of rbbExpr list

    (* Non-literals *)
    | RbbContextExpr
    | RbbMessageExpr
    | RbbSelfRefExpr
    | RbbSendMessageExpr of rbbExpr * rbbExpr

type rbbBlockSealState =
    | RbbBlockNotSealed
    | RbbBlockSealed

type rbbObject =
    | RbbEmpty
    | RbbNumber of int
    | RbbSymbol of string
    | RbbArray of rbbObject array
    | RbbObject of (rbbObject -> rbbObject)

    (* List of statements where the last statement is the return + context object *)
    | RbbBlock of {
        stms: rbbExpr list;
        parent_blk: rbbObject;
        mutable ctx: rbbObject;
        parent_msg: rbbObject;
        mutable seal_state: rbbBlockSealState
    }

exception NotImplemented
exception MessageNotRecognized of {description: string; receiver: rbbObject; message: rbbObject}
exception RuntimeError of rbbObject

(* {$0} if true
   {$1} if false *)
let rec rbb_boolean value =
    (RbbBlock {
        stms =
            [RbbSendMessageExpr(
                RbbMessageExpr,
                (RbbNumberExpr (if value then 0 else 1)))];
        ctx = RbbEmpty;
        parent_blk = RbbEmpty;
        parent_msg = RbbEmpty;
        seal_state = RbbBlockSealed
    })

and send_msg rcv msg =
    let not_recognized desc =
        raise (MessageNotRecognized {
            description = desc;
            receiver = rcv;
            message = msg
        }) in

    let unknown_op () = not_recognized "Unknown operation" in
    let obj_expected name = not_recognized (String.concat " " [name; "expected"]) in

    let obj_cmp op = RbbObject(fun operand ->
        rbb_boolean (op rcv operand)) in

    match rcv with
    | RbbEmpty -> begin match msg with
        | RbbSymbol(sym) -> begin match sym with
            | "==" -> obj_cmp (=)
            | "/=" -> obj_cmp (fun x y -> not (x = y))
            | _ -> unknown_op ()
            end
        | _ -> unknown_op ()
        end
    | RbbNumber(rcv_n) -> begin match msg with
        | RbbSymbol(msg_sym) ->
            let arith op = RbbObject(fun operand ->
                match operand with
                | RbbNumber(other_n) -> RbbNumber(op rcv_n other_n)
                | _ -> obj_expected "Number") in

            let num_cmp op = RbbObject(fun operand ->
                match operand with
                | RbbNumber(other_n) -> rbb_boolean (op rcv_n other_n)
                | _ -> obj_expected "Number")

            in begin match msg_sym with
            | "+" -> arith (+)
            | "-" -> arith (-)
            | "*" -> arith ( * )
            | "/" -> arith (/)
            | "mod" -> arith (mod)
            | "==" -> obj_cmp (=)
            | "/=" -> obj_cmp (fun x y -> not (x = y))
            | "<" -> num_cmp (<)
            | ">" -> num_cmp (>)
            | "<=" -> num_cmp (<=)
            | ">=" -> num_cmp (>=)
            | _ -> unknown_op ()
            end
        | _ -> obj_expected "Symbol"
        end
    | RbbSymbol(rcv_sym) -> begin match msg with
        | RbbSymbol(op) ->
            let cmp_sym eq = RbbObject(fun operand ->
                match operand with
                | RbbSymbol(other_sym) ->
                    rbb_boolean ((String.compare rcv_sym other_sym == 0) == eq)
                | _ -> rbb_boolean false) in

            let to_eq sym = match sym with
            | "==" -> true
            | "/=" -> false
            | _ -> unknown_op ()

            in cmp_sym (to_eq op)
        | _ -> obj_expected "Symbol"
        end
    | RbbArray(objects) -> begin match msg with
        | RbbNumber(idx) -> get objects idx
        | RbbSymbol(op) -> begin match op with
            | "len" -> RbbNumber(Array.length objects)
            | "concat" -> RbbObject(fun other ->
                match other with
                | RbbArray(other_objects) -> RbbArray(Array.append objects other_objects)
                | _ -> obj_expected "Array")
            | _ -> unknown_op ()
            end
        | RbbArray(params) ->
            if Array.length params = 2 then
                let idx = get params 0 in
                let new_value = get params 1 in

                begin match idx with
                | RbbNumber(idx_n) -> set objects idx_n new_value; rcv (* return array itself *)
                | _ -> not_recognized "Index should be a number"
                end
            else
                not_recognized "Wrong number of parameters"
        | _ -> not_recognized ""
        end
    | RbbObject(f) -> (f msg)
    | RbbBlock(block_rec) ->
        let eval_blk block ctx msg =
            List.fold_left
                (fun _ expr -> expr_eval block ctx msg expr)
                RbbEmpty
                block_rec.stms in

        match block_rec.seal_state with
        | RbbBlockSealed -> eval_blk rcv block_rec.ctx msg
        | RbbBlockNotSealed ->
            begin match msg with
            | RbbSymbol(msg_sym) ->
                begin match msg_sym with
                | "clone" -> RbbBlock{
                    stms = block_rec.stms;
                    parent_blk = block_rec.parent_blk;
                    ctx = block_rec.ctx;
                    parent_msg = block_rec.parent_msg;
                    seal_state = block_rec.seal_state;
                }
                | "context" -> block_rec.ctx
                | "exec" ->
                    eval_blk block_rec.parent_blk block_rec.ctx block_rec.parent_msg
                | "seal" -> block_rec.seal_state <- RbbBlockSealed; rcv
                | _ -> unknown_op ()
                end
            | RbbArray(args) ->
                begin match Array.to_list args with
                | [RbbSymbol("context"); new_ctx] ->
                    block_rec.ctx <- new_ctx; rcv
                | _ -> unknown_op ()
                end
            | _ -> unknown_op ()
            end

and expr_eval block context msg expr =
    let cur_expr_eval = expr_eval block context msg in

    match expr with
    | RbbEmptyExpr -> RbbEmpty
    | RbbNumberExpr(n) -> RbbNumber(n)
    | RbbSymbolExpr(s) -> RbbSymbol(s)
    | RbbArrayExpr(arr) -> RbbArray(of_list (map (fun expr -> cur_expr_eval expr) arr))
    | RbbBlockExpr(stms) -> RbbBlock{
        stms; parent_blk = block; ctx = context; parent_msg = msg; seal_state = RbbBlockNotSealed}
    | RbbContextExpr -> context
    | RbbMessageExpr -> msg
    | RbbSelfRefExpr -> block
    | RbbSendMessageExpr(rcv_expr, msg_expr) ->
        send_msg (cur_expr_eval rcv_expr) (cur_expr_eval msg_expr)

let (#$) rcv msg = send_msg rcv msg
let (#$$) rcv_expr msg_expr = RbbSendMessageExpr(rcv_expr, msg_expr)

(* Helpers for parse_expr *)
let explode s =
    let rec exp i l =
        if i < 0 then l else exp (i - 1) (s.[i] :: l) in
    exp (String.length s - 1) []

let implode l =
    List.fold_left (fun str ch -> str ^ String.make 1 ch) "" l

let rec assoc rexpr_l =
    match rexpr_l with
    | [] -> None
    | e2 :: e1 :: [] -> Some(e1 #$$ e2)
    | e :: rtail ->
        match (assoc rtail) with
        | Some(tail_e) -> Some(tail_e #$$ e)
        | None -> Some(e)

let assoc expr_l = assoc (List.rev expr_l)

let rec strip_front_spaces ch_l =
    match ch_l with
    | ' ' :: tail -> strip_front_spaces tail
    | _ -> ch_l

let rec split_num ch_l top =
    match ch_l with
    | '0' .. '9' as h :: t -> split_num t (h :: top)
    | _ -> (List.rev top, ch_l)

let split_num ch_l = split_num ch_l []

let rec split_sym ch_l top =
    let app ch t = split_sym t (ch :: top) in

    match ch_l with
    | 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' as h :: t -> app h t
    | _ -> (List.rev top, ch_l)

let split_sym ch_l = split_sym ch_l []

let special_chars = explode "*+-/<=>?\\^"
let rec is_special_char ch ch_l =
    match ch_l with
    | h :: t -> if h == ch then true else is_special_char ch t
    | _ -> false
let is_special_char ch = is_special_char ch special_chars

let rec split_special_sym ch_l top =
    let app ch t = split_special_sym t (ch :: top) in

    match ch_l with
    | h :: t -> if is_special_char h then app h t else (List.rev top, ch_l)
    | _ -> (List.rev top, ch_l)
let split_special_sym ch_l = split_special_sym ch_l []

exception SyntaxError of string

let rec parse_expr char_list =
    let (elist, rest) = parse_msg_send char_list in (assoc elist, rest)

and parse_parens char_list =
    match char_list with
    | '(' :: ')' :: rest -> (Some RbbEmptyExpr, rest)
    | '(' :: tail ->
        let (subexpr, rest) = parse_expr tail in

        begin match rest with
        | ')' :: rest_ -> (subexpr, rest_)
        | _ -> raise (SyntaxError "Unclosed parens")
        end
    | _ -> (None, char_list)

and parse_number char_list =
    match char_list with
    | '0' .. '9' as h :: tail ->
        let (num_str, rest) = split_num (h :: tail) in
        let num = int_of_string (implode num_str) in

        (Some(RbbNumberExpr num), rest)
    | _ -> (None, char_list)

and parse_symbol char_list =
    match char_list with
    | 'a' .. 'z' | 'A' .. 'Z' | '_' as h :: tail ->
        let (sym, rest) = split_sym (h :: tail) in

        (Some(RbbSymbolExpr (implode sym)), rest)
    | h :: tail ->
        if is_special_char h then
            let (sym, rest) = split_special_sym (h :: tail) in

            (Some(RbbSymbolExpr (implode sym)), rest)
        else
            (None, char_list)
    | _ -> (None, char_list)

and parse_array char_list =
    match char_list with
    | '[' :: tail ->
        let (elist, rest) = parse_array_body tail in

        begin match rest with
        | ']' :: tail_ -> (Some(RbbArrayExpr elist), tail_)
        | _ -> raise (SyntaxError "Unclosed bracket")
        end
    | _ -> (None, char_list)

and parse_array_body char_list =
    let (expr, rest) = parse_expr char_list in

    match expr with
    | Some(e) -> 
        begin match rest with
        | ',' :: rest_ ->
            let (elist, rest__) = parse_array_body rest_ in

            (e :: elist, rest__)
        | _ -> ([e], rest)
        end
    | None -> ([], char_list)

and parse_function_params char_list =
    match char_list with
    | '$' :: tail -> (Some RbbMessageExpr, tail)
    | '~' :: tail -> (Some RbbContextExpr, tail)
    | '@' :: tail -> (Some RbbSelfRefExpr, tail)
    | _ -> (None, char_list)

and parse_msg_send char_list =
    let rules = [
        parse_parens;
        parse_number;
        parse_symbol;
        parse_array;
        parse_function_params;
        parse_block
    ] in
    
    let char_list = strip_front_spaces char_list in

    let (expr, rest) = List.fold_left (fun v f ->
        match v with
        | (None, _) -> f char_list
        | (Some(_), _) -> v
    ) (None, char_list) rules in

    match expr with
    | Some(e) ->
        let (elist, rest_) = parse_msg_send rest in (e :: elist, rest_)
    | None -> ([], char_list)

and parse_block_body char_list =
    let (fst_expr, rest) = parse_expr char_list in

    match fst_expr with
    | None -> ([], char_list)
    | Some(e) ->
        let (elist, rest_) = parse_rem_block_body rest in (e :: elist, rest_)

and parse_rem_block_body char_list =
    match strip_front_spaces char_list with
    | ';' :: tail ->
        let (expr, rest) = parse_expr tail in

        begin match expr with
        | None -> ([], char_list)
        | Some(e) ->
            let (elist, rest_) = parse_rem_block_body rest in

            (e :: elist, rest_)
        end
    | _ -> ([], char_list)

and parse_block char_list =
    match strip_front_spaces char_list with
    | '{' :: tail ->
        let (body, rest) = parse_block_body tail in

        begin match strip_front_spaces rest with
        | '}' :: tail -> (Some(RbbBlockExpr(body)), tail)
        | _ -> raise (SyntaxError "Missing closing curly bracket")
        end
    | _ -> (None, char_list)

let parse_expr str = parse_expr (explode str)

(* Useful utility *)
let eval_code code =
    match parse_expr code with
    | (Some(expr), _) ->
        expr_eval RbbEmpty RbbEmpty RbbEmpty expr
    | _ -> RbbEmpty
